package br.edu.utfpr.pmp3;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class PlayListActivity extends Activity implements OnItemClickListener {

	private ListView diretorios;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playlist);

		diretorios = (ListView) findViewById(R.id.lstSDCard);
		diretorios.setOnItemClickListener(this);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.list_item);

		File path = Environment.getExternalStorageDirectory();
		if (path.exists()) {
			File[] files = path.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					String dir = file.getName();
					if (!dir.contains(".")) {
						adapter.add(dir);
					}
				}
			}
		}
		diretorios.setAdapter(adapter);
	}

	public void onItemClick(AdapterView<?> adapter, View v, int position, long l) {
		ArrayList<String> arquivos = new ArrayList<String>();
		String pasta = (String) adapter.getItemAtPosition(position);
		File path = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/" + pasta);
		if (path.exists()) {
			File[] files = path.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						String nome = file.getName();
						String[] array = nome.split("\\.");
						if (nome != null && array.length > 1
								&& array[1].equals("mp3"))
							arquivos.add(array[0]);
					}

				}
			}
		}

		if (arquivos.size() > 0) {
			Collections.sort(arquivos);
			Intent intent = getIntent();
			intent.putExtra("pasta", pasta);
			intent.putExtra("arquivos", arquivos);
			setResult(0, intent);
			finish();
		} else {
			Toast.makeText(this, "Nenhum MP3 encontrado!", Toast.LENGTH_SHORT)
					.show();
		}
	}

}
