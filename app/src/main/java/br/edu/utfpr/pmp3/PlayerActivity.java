package br.edu.utfpr.pmp3;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

@SuppressWarnings("unchecked")
public class PlayerActivity extends Activity implements OnItemClickListener,
		OnCompletionListener, OnTouchListener {

	private static String EXEMPLO = "Exemplo";
	private MediaPlayer player;
	private Button btnPlayOuPause;
	private SeekBar barraDeProgresso;
	private ListView playlist;
	private int posicao;
	private File path;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);
		btnPlayOuPause = (Button) findViewById(R.id.btnPlayer);
		barraDeProgresso = (SeekBar) findViewById(R.id.skbProgresso);
		barraDeProgresso.setOnTouchListener(this);
		playlist = (ListView) findViewById(R.id.lstPlaylist);
		playlist.setOnItemClickListener(this);
		playlist.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item,
				Arrays.asList(EXEMPLO)));
	}

	public void tocarOuPausar(View v) {
		if (player == null) {
			definirMusica();
		} else {
			if (player.isPlaying()) {
				pausar();
			} else {
				reiniciar();
			}
		}
	}

	private void tocar(File path, String musica) {
		if (musica != null) {
			try {
				if (musica.equals(EXEMPLO)) {
					player = MediaPlayer.create(this, R.raw.exemplo);
				} else {
					player = new MediaPlayer();
					player.setOnCompletionListener(this);
					String src = path.getAbsolutePath() + "/" + musica + ".mp3";
					player.setDataSource(src);
					player.prepare();
				}
				barraDeProgresso.setMax(player.getDuration());
				barraDeProgresso.setVisibility(View.VISIBLE);
				player.start();
				btnPlayOuPause.setText(R.string.pausar);
				atualizarProgresso();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean onTouch(View view, MotionEvent event) {
		if (player != null && player.isPlaying()) {
			SeekBar barra = (SeekBar) view;
			player.seekTo(barra.getProgress());
		}
		return false;
	}

	public void parar(View v) {
		if (player != null) {
			player.stop();
			player.release();
			player = null;
			barraDeProgresso.setVisibility(View.INVISIBLE);
			barraDeProgresso.setProgress(0);
			btnPlayOuPause.setText(R.string.tocar);
		}
	}

	public void onItemClick(AdapterView<?> adapter, View v, int posicao, long l) {
		if (adapter.getCount() > 0) {
			this.posicao = posicao;
			String musica = (String) adapter.getItemAtPosition(posicao);
			parar(null);
			tocar(path, musica);
		}
	}

	public void onCompletion(MediaPlayer player) {
		this.posicao++;
		definirMusica();
	}

	private void definirMusica() {
		String musica = null;
		barraDeProgresso.setVisibility(View.INVISIBLE);
		barraDeProgresso.setProgress(0);
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) playlist
				.getAdapter();
		if (posicao >= 0 && posicao < adapter.getCount()) {
			musica = adapter.getItem(posicao);
			if (musica != null) {
				tocar(path, musica);
			}
		} else {
			if (posicao < 0) {
				Toast.makeText(this, "Não é possível voltar mais!",
						Toast.LENGTH_LONG).show();
				posicao = 0;
			} else if (posicao >= adapter.getCount()) {
				Toast.makeText(this, "Não é possível avançar mais!",
						Toast.LENGTH_LONG).show();
				posicao = adapter.getCount() - 1;
			}
		}
	}

	public void atualizarProgresso() {
		if (player != null) {
			barraDeProgresso.setProgress(player.getCurrentPosition());
			if (player.isPlaying()) {
				Runnable runnable = new Runnable() {
					public void run() {
						atualizarProgresso();
					}
				};
				new Handler().postDelayed(runnable, 1000);
			}
		}
	}

	public void escolherPlaylist(View v) {
		String state = Environment.getExternalStorageState();
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			Intent intent = new Intent(this, PlayListActivity.class);
			startActivityForResult(intent, 0);
		} else {
			Toast.makeText(this, "Dispositivo sem SDCard!", Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			String pasta = data.getStringExtra("pasta");
			path = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/" + pasta);
			ArrayList<String> arquivos = (ArrayList<String>) data
					.getSerializableExtra("arquivos");
			playlist.setAdapter(new ArrayAdapter<String>(this,
					R.layout.list_item, arquivos));
		}
	}

	public void voltarAoInicio(View v) {
		this.posicao--;
		parar(null);
		definirMusica();

	}

	public void irParaFinal(View v) {
		this.posicao++;
		parar(null);
		definirMusica();
	}

	private void reiniciar() {
		if (player != null) {
			player.start();
			btnPlayOuPause.setText(R.string.pausar);
		}
	}

	private void pausar() {
		if (player != null && player.isPlaying()) {
			player.pause();
			btnPlayOuPause.setText(R.string.tocar);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		pausar();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		reiniciar();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		parar(null);
	}
}
